// Copyright 2020 William Salmon.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



use std::marker::PhantomData;

use druid::{
    widget::{Flex, Svg, SvgData, WidgetExt, Slider},
    AppLauncher, Widget, WindowDesc, Data, Lens, Env, PaintCtx, BoxConstraints, LayoutCtx, Size, UpdateCtx, Event, EventCtx
};


use plotlib::scatter::Scatter;
use plotlib::scatter;
use plotlib::style::{Marker, Point};
use plotlib::page::Page;

use std::str::FromStr;

//use svg::write_internal;

#[derive(Clone, Data, Lens, Debug)]
struct FullData {
    svg: f64,
    slider: f64,
}


/// A widget that renders a SVG
pub struct Plotter<T> {
    svg_widget: Box<dyn Widget<u32>>,
    phantom: PhantomData<T>,
}

impl Plotter<f64> {
    /// Create an SVG-drawing widget from SvgData.
    ///
    /// The SVG will scale to fit its box constraints.
    pub fn new(svg_data: SvgData) -> impl Widget<f64> {
        Plotter {
            svg_widget: Box::new(Svg::new(svg_data)),
            phantom: Default::default(),
        }
    }
}

impl Widget<f64> for Plotter<f64> {
    fn event(&mut self, _ctx: &mut EventCtx, _event: &Event, _data: &mut f64, _env: &Env) {}

    fn update(&mut self, _ctx: &mut UpdateCtx, _old_data: Option<&f64>, data: &f64, _env: &Env) {
        // i plan to put code hear,
        // probaly changing
        // self.svgWidet = new widget with new svg

        println!("update {:?}", data);
        let newsvg = make_svg(*data);
        self.svg_widget = Box::new(Svg::new(newsvg));

    }

    fn layout(
        &mut self,
        layout_ctx: &mut LayoutCtx,
        bc: &BoxConstraints,
        _data: &f64,
        env: &Env,
    ) -> Size {
        self.svg_widget.layout(layout_ctx, bc, &0, env)
    }

    fn paint(&mut self, paint_ctx: &mut PaintCtx, _data: &f64, env: &Env) {
        self.svg_widget.paint(paint_ctx, &0, env);
    }
}




fn main() {
    let main_window = WindowDesc::new(ui_builder);
    let data = FullData {svg: 0.5, slider: 0.5};
    AppLauncher::with_window(main_window)
        .use_simple_logger()
        .launch(data)
        .expect("launch failed");
}

fn make_svg(input_x: f64) -> SvgData {
    let data1 =[(1., 2.0), (3.0, 3.0 * input_x)];
    let data2 =[(2., 2.0), (4.0, 3.0)];
    let s1 = Scatter::from_slice(&data1)
        .style(scatter::Style::new()
            .marker(Marker::Square) // setting the marker to be a square
            .colour("#DD3355")); // and a custom colour
    let s2 = Scatter::from_slice(&data2)
            .style(scatter::Style::new() // uses the default marker
            .colour("#35C788")); // and a different colour
    

    // The 'view' describes what set of data is drawn
    let v = plotlib::view::ContinuousView::new()
        .add(&s1)
        .add(&s2)
        .x_range(0., 5.)
        .y_range(0., 5.)
        .x_label("Some varying variable")
        .y_label("The response of something");

    // A page with a single view is then saved to an SVG file
    //Page::single(&v).save("scatter.svg");

    let svg = Page::single(&v).to_svg().expect("Woah");
    let mut vec_svg = Vec::new();
    svg::write(&mut vec_svg, &svg).unwrap();
    let str_svg = String::from_utf8_lossy(&vec_svg);

    let tiger_svg: SvgData = match SvgData::from_str(&str_svg){
        Err(why) => panic!("couldn't open {}", why),
        Ok(tiger_svg) => tiger_svg,
    };
    tiger_svg
}

fn ui_builder() -> impl Widget<FullData> {

 
    let tiger_svg = make_svg(0.5);


    let mut col = Flex::column();
    let slider = Slider::new().lens(FullData::slider);

    //col.add_child(Svg::new(tiger_svg.clone()).fix_width(100.0).center(), 1.0);
    col.add_child(Plotter::new(tiger_svg).lens(FullData::slider), 1.0);
    col.add_child(slider, 0.0);
    col
}
